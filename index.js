
//2. Create a database of hotel with a collection of rooms

// 3. Insert a single room(insertOne method) with the following details:

db.rooms.insertOne({
	"name": "Single",
	"accomodate": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"room_available": 10,
	"isAvailable": false
})
//3. Insert multiple rooms(insertMany method) with the following details:

db.rooms.insertMany([
{

	"name": "Double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small going on a vacation",
	"room_available": 5,
	"isAvailable": false
},
// Insert multiple rooms(insertMany method) with the following details.
{

	"name": "Queen",
	"accomodates": 3,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"room_available": 15,
	"isAvailable": false
}
])

// 5. Use Find method to search for a room with the name double.

db.rooms.find({"name": "Double"})

// 6. Use the updateOne method to update the queen room and set the available rooms to 0


db.rooms.updateOne(
	{"name": "Queen"},
		{
			$set: {
				"room_available": 0,
				}
		}
)

// 7. Use the deleteMany method rooms to delete all rooms that have 0 availabilty.

db.rooms.deleteMany({
	"name": "Queen"
})




